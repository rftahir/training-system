
# Training system

The Training System for Country Delegation will control the training activities for each skill where this country will participate in a competition.
## Tech Stack

**Client:** React, NextJS, TailwindCSS

**Server:** Node, Fastify

**Database:** PostgreSQL 15.3, Redis


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/rftahir/training-system
```

Go to the project directory

```bash
  cd training-system
```

### Database
This project has docker-compose file included for database. Just run it with
```bash
  docker-compose up -d
```
or
```bash
  docker compose up -d
```


### Backend

Go to backend directory
```bash
  cd backend
```

Install dependencies

```bash
  npm install
```

Copy environment variables

```bash
  cp .env.example .env
```
Then make adjusment for the configuration.

Start the server

```bash
  npm run dev
```

Application will running on port 8000 by default.

### Frontend

Go to frontend directory
```bash
  cd frontend
```

Install dependencies

```bash
  npm install
```
Copy environment variables

```bash
  cp .env.example .env
```
Then make adjusment for the configuration.

Start the server

```bash
  npm run dev
```

Application will running on port 3000 by default.


## Running Tests

You can use following username for testing:
 - john_doe (board)
 - jsmith21 (expert)
 - aroberts10 (competitor)
 - mjohnson89 (trainer)

those accounts has same password: 1234ABCD

