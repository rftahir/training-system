import NextAuth from "next-auth";
import Credentials from "next-auth/providers/credentials";
import axios from "axios";

export const authOptions = {
  providers: [
    Credentials({
      name: "Credentials",
      credentials: {
        username: { label: "Username", type: "text", placeholder: "jsmith" },
        password: { label: "Password", type: "password" }
      },
      async authorize(credentials, req) {
        let user = null;

        await axios.post(`${process.env.API_URL}/auth/login`, { username: req.body.username, password: req.body.password })
        .then((response) => {
          user = response.data.data;
          return
        }).catch((err) => {
          return null
        });

        return user;
      }
    }),
  ],
  secret: process.env.SECRET_KEY,
  session: {
    strategy: 'jwt',
    maxAge: 30 * 24 * 60 * 60, // 30 Days
  },
  pages: {
    signIn: '/',
    signOut: '/',
  },
  callbacks: {
    async jwt({ token, user, account }) {
      if (account) {
        token.account = { 
          ...account, 
          access_token: user.token,
          profile: user.profile
        };
      }
      return token;
    },
    async session({ session, token }) {

      return { ...session, accessToken: token.account.access_token, profile: token.account.profile };
    },
  }
}

export default NextAuth(authOptions)
