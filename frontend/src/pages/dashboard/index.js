import { useEffect, useState } from "react";
import { UserIcon, ArrowSmallRightIcon, ArrowSmallLeftIcon, PlusIcon } from '@heroicons/react/24/outline'
import { useSession } from "next-auth/react"
import { useRouter } from "next/router";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import axios from "axios";
import dayjs from 'dayjs';

import Layout from "@/components/Layout";

const Dashboard = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage, setPerPage] = useState(9);
  const [totalPage, setTotalPage] = useState(null);
  const [activities, setActivities] = useState([]);
  const [token, setToken] = useState(null);
  const { data: session } = useSession();
  const router = useRouter();
  const swal = withReactContent(Swal)

  useEffect(() => {
    if(session) setToken(session.accessToken)
  }, [session]);

  useEffect(() => {
    if(currentPage && perPage && token) fetchData()
  }, [currentPage, perPage, token])

  const handleNext = () => {
    setCurrentPage(currentPage + 1);
  }

  const handlePrevious = () => {
    setCurrentPage(currentPage - 1);
  }

  const fetchData =  () => {
    axios({
      method: 'get',
      url: `${process.env.NEXT_PUBLIC_API_URL}/activity?perPage=${perPage}&currentPage=${currentPage}`,
      headers: {
        "Authorization": `Bearer ${token}`,
      }
    })
    .then((resp) => {
      const data = resp.data;
      setActivities(data.data);

      if(data.lastPage) setTotalPage(data.lastPage);
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch data</p>,
        icon: 'error'
      })
    })
  }


  return (
    <Layout>
      <div className="w-full text-black dark:text-white flex items-center justify-between">
        <span className="text-2xl font-bold">List of Activity</span>
        { session?.profile == 'expert' &&
          <button 
            onClick={() => {
              router.push('/dashboard/activities/create');
            }} 
            className="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2"
          >
            <PlusIcon className="mr-2 h-6 w-6" aria-hidden="true"  />
            Add new Activity
          </button>
        }
      </div>
      <div className="flex flex-wrap items-stretch my-4">
        { activities.map((val, idx) => (
          <div className="w-full lg:w-1/3 lg:pr-4 my-2 flex"  key={`activity-${idx}`}>
            <div 
              onClick={() => router.push(`/dashboard/activities/${val.id}`)}
              className="bg-gray-50 rounded-xl p-6 shadow-lg text-black grow flex items-start justify-between flex-col cursor-pointer hover:bg-gray-300"
            >
              <div>
                <span className="text-xl font-bold mb-4">{val.title}</span>
                <div className="text-sm leading-relaxed">{val.description.length > 200 ? val.description.replace(/(<([^>]+)>)/gi, "").substring(0,200)+`...` : val.description}</div>
              </div>
              <div className="flex items-center justify-between w-full">
                <span className="inline-flex items-center rounded-md bg-indigo-50 px-2 py-1 text-xs font-medium text-indigo-700 ring-1 ring-inset ring-indigo-700/10 mt-2">
                  {dayjs(val.startdate).format('MMMM D, YYYY')} - {dayjs(val.enddate).format('MMMM D, YYYY')} 
                </span>
                <div className="flex">
                  <UserIcon className="h-6 w-6" aria-hidden="true" />
                  {val.participants.length}
                </div>
              </div>
            </div>
          </div>
          ))
        }
      </div>
      <div className="flex items-center justify-center p-2 my-2">
          { currentPage != 1 &&
            <button onClick={handlePrevious} className="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2">
              <ArrowSmallLeftIcon className="mr-2 h-6 w-6" aria-hidden="true"  />
              Previous
            </button>
          }
          { currentPage != totalPage &&
            <button onClick={handleNext} className="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2">
              Next
              <ArrowSmallRightIcon className="ml-2 h-6 w-6" aria-hidden="true"  />
            </button>
          }
      </div>
    </Layout>
  )
}

export default Dashboard;