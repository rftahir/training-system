import { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { CheckCircleIcon } from "@heroicons/react/24/outline";
import DatePicker from "react-datepicker";
import axios from "axios";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import dayjs from 'dayjs';

import Layout from "@/components/Layout";

import "react-datepicker/dist/react-datepicker.css";
import Unauthorized from "@/components/Unauthorized";

const availableProfile = [
  {value: "board", label: "Board"},
  {value: "expert", label: "Expert"},
  {value: "trainer", label: "Trainer"},
  {value: "competitor", label: "Competitor"},
]

const RegisterUser = () => {
  const [passwordFieldType, setPasswordFieldType] = useState('password');
  const [token, setToken] = useState(null);
  const [skills, setSkills] = useState([]);
  const [formData, setFormData] = useState({
    name: null,
    email: null,
    skill: null,
    username: null,
    password: null,
    profile: null
  })
  const { data: session } = useSession();
  const swal = withReactContent(Swal);
  const router = useRouter();

  useEffect(() => {
    if(session) setToken(session.accessToken)
  }, [session]);

  useEffect(() => {
    if(token)
      fetchSkills()
  }, [token])

  const fetchSkills =  () => {
    axios({
      method: 'get',
      url: `${process.env.NEXT_PUBLIC_API_URL}/skills?allData=true`,
      headers: {
        "Authorization": `Bearer ${token}`,
      }
    })
    .then((resp) => {
      const data = resp.data;
      setSkills(data.data);
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch data</p>,
        icon: 'error'
      })
    })
  }


  const handleSubmit = () => {
    axios({
      method: 'post',
      url: `${process.env.NEXT_PUBLIC_API_URL}/users`,
      headers: {
        "Authorization": `Bearer ${token}`,
      },
      data: formData
    })
    .then((resp) => {
      swal.fire({
        title: <p>Data Submitted</p>,
        icon: 'success'
      }).then(() => {
        router.push('/dashboard/users')
      })
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to process data</p>,
        icon: 'error'
      })
    })
  }

  if(session?.profile != 'board') return <Unauthorized />


  return (
    <Layout>
      <div className="w-full text-black dark:text-white flex items-center justify-between">
        <span className="text-2xl font-bold">Register New User</span>
      </div>
      <div className="w-full my-4 rounded-xl bg-gray-50 p-4 text-black">
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Name</label>
          <input
            type="text"
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, name: event.target.value})
            }}
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Email</label>
          <input
            type="email"
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, email: event.target.value})
            }}
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Username</label>
          <input
            type="text"
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, username: event.target.value})
            }}
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3 relative flex items-start justify-center flex-col">
          <label className="font-medium leading-none mb-1">Password</label>
          <input
            type={passwordFieldType}
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, password: event.target.value})
            }}
          />
          <div className="absolute right-0 mt-2 mr-3 cursor-pointer" onClick={() => {
            if(passwordFieldType == 'text'){
              setPasswordFieldType('password');
            }else{
              setPasswordFieldType('text');
            }
          }}>
            <svg
              width="16"
              height="16"
              viewBox="0 0 16 16"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M7.99978 2C11.5944 2 14.5851 4.58667 15.2124 8C14.5858 11.4133 11.5944 14 7.99978 14C4.40511 14 1.41444 11.4133 0.787109 8C1.41378 4.58667 4.40511 2 7.99978 2ZM7.99978 12.6667C9.35942 12.6664 10.6787 12.2045 11.7417 11.3568C12.8047 10.509 13.5484 9.32552 13.8511 8C13.5473 6.67554 12.8031 5.49334 11.7402 4.64668C10.6773 3.80003 9.35864 3.33902 7.99978 3.33902C6.64091 3.33902 5.32224 3.80003 4.25936 4.64668C3.19648 5.49334 2.45229 6.67554 2.14844 8C2.45117 9.32552 3.19489 10.509 4.25787 11.3568C5.32085 12.2045 6.64013 12.6664 7.99978 12.6667ZM7.99978 11C7.20413 11 6.44106 10.6839 5.87846 10.1213C5.31585 9.55871 4.99978 8.79565 4.99978 8C4.99978 7.20435 5.31585 6.44129 5.87846 5.87868C6.44106 5.31607 7.20413 5 7.99978 5C8.79543 5 9.55849 5.31607 10.1211 5.87868C10.6837 6.44129 10.9998 7.20435 10.9998 8C10.9998 8.79565 10.6837 9.55871 10.1211 10.1213C9.55849 10.6839 8.79543 11 7.99978 11ZM7.99978 9.66667C8.4418 9.66667 8.86573 9.49107 9.17829 9.17851C9.49085 8.86595 9.66644 8.44203 9.66644 8C9.66644 7.55797 9.49085 7.13405 9.17829 6.82149C8.86573 6.50893 8.4418 6.33333 7.99978 6.33333C7.55775 6.33333 7.13383 6.50893 6.82126 6.82149C6.5087 7.13405 6.33311 7.55797 6.33311 8C6.33311 8.44203 6.5087 8.86595 6.82126 9.17851C7.13383 9.49107 7.55775 9.66667 7.99978 9.66667Z"
                fill="#71717A"
              />
            </svg>
          </div>
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Profile</label>
          <select
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, profile: event.target.value})
            }}
          > 
            { availableProfile.map((val, idx) => (
                <option value={val.value} key={`value-${val.value}`}>{val.label}</option>
              ))
            }
          </select>
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Skill</label>
          <select 
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, skill: event.target.value})
            }}
          >
            { skills.map((val, idx) => (
                <option key={idx} value={val.id}>{val.skill_name}</option>
              ))
            }
          </select>
        </div>
        <div className="w-full mt-3 flex items-center justify-end">
          <button onClick={handleSubmit} className="bg-green-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2">
            Save
            <CheckCircleIcon className="ml-2 h-6 w-6" aria-hidden="true"  />
          </button>
        </div>
      </div>
    </Layout>
  )
}

export default RegisterUser;