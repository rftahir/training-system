import { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

import Layout from "@/components/Layout";
import Unauthorized from "@/components/Unauthorized";
import { ArrowSmallLeftIcon, ArrowSmallRightIcon, PlusIcon } from "@heroicons/react/24/outline";


const Users = () => {
  const [users, setUsers] = useState([]);
  const { data: session } = useSession();
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage, setPerPage] = useState(9);
  const [totalPage, setTotalPage] = useState(null);
  const [token, setToken] = useState(null);
  const router = useRouter();
  const swal = withReactContent(Swal)

  useEffect(() => {
    if(session) setToken(session.accessToken)
  }, [session]);

  useEffect(() => {
    if(currentPage && perPage && token) fetchData()
  }, [currentPage, perPage, token]);

  const fetchData =  () => {
    axios({
      method: 'get',
      url: `${process.env.NEXT_PUBLIC_API_URL}/users?perPage=${perPage}&currentPage=${currentPage}`,
      headers: {
        "Authorization": `Bearer ${token}`,
      }
    })
    .then((resp) => {
      const data = resp.data;
      setUsers(data.data);

      if(data.lastPage) setTotalPage(data.lastPage);
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch data</p>,
        icon: 'error'
      })
    })
  }

  const handleNext = () => {
    setCurrentPage(currentPage + 1);
  }

  const handlePrevious = () => {
    setCurrentPage(currentPage - 1);
  }


  if(session?.profile != 'board') return <Unauthorized />  
  return (
    <Layout>
      <div className="w-full text-black dark:text-white flex items-center justify-between">
        <span className="text-2xl font-bold">List of Users</span>
        <button 
          onClick={() => {
            router.push('/dashboard/users/create');
          }} 
          className="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2"
        >
          <PlusIcon className="mr-2 h-6 w-6" aria-hidden="true"  />
          Register New User
        </button>
      </div>
      <div className="w-full text-black dark:text-white flex items-center justify-between my-4">
        <table className="table-auto w-full">
          <thead className="border-b">
            <tr className="bg-gray-50 text-black">
              <th className="text-left p-4 font-medium">Name</th>
              <th className="text-left p-4 font-medium">Email</th>
              <th className="text-left p-4 font-medium">Username</th>
              <th className="text-left p-4 font-medium">Skill</th>
              <th className="text-left p-4 font-medium">Profile</th>
            </tr>
          </thead>
          <tbody>
            { users.map((val, idx) => (
                <tr className="border-b hover:bg-gray-50 hover:text-black" key={`table-${val.id}`}>
                  <td className="p-4">{val.name}</td>
                  <td className="p-4">{val.email}</td>
                  <td className="p-4">{val.username}</td>
                  <td className="p-4">{val.skill}</td>
                  <td className="p-4">{val.profile}</td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
      <div className="flex items-center justify-center p-2 my-2">
          { currentPage != 1 &&
            <button onClick={handlePrevious} className="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2">
              <ArrowSmallLeftIcon className="mr-2 h-6 w-6" aria-hidden="true"  />
              Previous
            </button>
          }
          { currentPage != totalPage &&
            <button onClick={handleNext} className="bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2">
              Next
              <ArrowSmallRightIcon className="ml-2 h-6 w-6" aria-hidden="true"  />
            </button>
          }
      </div>
    </Layout>
  )
}

export default Users;