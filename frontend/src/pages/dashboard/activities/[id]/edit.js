import { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { getServerSession } from "next-auth/next"
import { useRouter } from "next/router";
import { CheckCircleIcon } from "@heroicons/react/24/outline";
import { authOptions } from "../../../api/auth/[...nextauth]";
import DatePicker from "react-datepicker";
import AsyncSelect from 'react-select/async';
import axios from "axios";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import dayjs from 'dayjs';

import Layout from "@/components/Layout";

import "react-datepicker/dist/react-datepicker.css";
import Unauthorized from "@/components/Unauthorized";

export async function getServerSideProps(context) {
  const session = await getServerSession(context.req, context.res, authOptions)

  try {
    const activity =  await axios({
                        method: "get",
                        url: `${process.env.NEXT_PUBLIC_API_URL}/activity/${context.params.id}`,
                        headers: {
                          Authorization: `Bearer ${session.accessToken}`,
                        },
                      });
    return {
      props : {
        activity: activity.data.data
      }
    }
  } catch(error) {
    console.log('error: ', error)
    return {
      props : {
        error: true
      }
    }
  }  
}

const CreateNewActivity = ({ activity, error }) => {
  const [token, setToken] = useState(null);
  const [skills, setSkills] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [formData, setFormData] = useState({
    title: activity.title,
    description: activity.description,
    skill: activity.skill,
    startdate: dayjs(activity.startdate).format('YYYY-MM-DD'),
    enddate: dayjs(activity.enddate).format('YYYY-MM-DD'),
    participants: activity.participants.map(item => item.id)
  })

  const { data: session } = useSession();
  const swal = withReactContent(Swal);
  const router = useRouter();

  useEffect(() => {
    if(session) setToken(session.accessToken)
  }, [session]);

  
  useEffect(() => {
    if(token) fetchSkills();
  }, [token])

  const fetchSkills =  () => {
    axios({
      method: 'get',
      url: `${process.env.NEXT_PUBLIC_API_URL}/skills?allData=true`,
      headers: {
        "Authorization": `Bearer ${token}`,
      }
    })
    .then((resp) => {
      const data = resp.data;
      setSkills(data.data);
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch skill data</p>,
        icon: 'error'
      })
    })
  }

  const fetchUser = async (q) => {
    var userData = [];

    if(session){

      await axios({
        method: 'get',
        url: `${process.env.NEXT_PUBLIC_API_URL}/users?perPage=10&q=${q}`,
        headers: {
          "Authorization": `Bearer ${token}`,
        }
      })
      .then((resp) => {
        const data = resp.data.data;

        data.map((val) => {
          userData.push({
            value: val.id,
            label: `${val.name}`
          })
        })
      })
      .catch((err) => {
        console.log(err)
        swal.fire({
          title: <p>Failed to fetch user data</p>,
          icon: 'error'
        })
      })
    }
    return userData;
  }

  const loadOptions = (inputValue, callback) => {
    setTimeout(async () => {
      const data = await fetchUser(inputValue);
      callback(data)
    }, 1000)
  }


  const handleSubmit = () => {
    axios({
      method: 'put',
      url: `${process.env.NEXT_PUBLIC_API_URL}/activity/${activity.id}`,
      headers: {
        "Authorization": `Bearer ${token}`,
      },
      data: formData
    })
    .then((resp) => {
      swal.fire({
        title: <p>Data Updated</p>,
        icon: 'success'
      }).then(() => {
        router.push(`/dashboard/activities/${activity.id}`)
      })
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch data</p>,
        icon: 'error'
      })
    })
  }

  if(session?.profile != 'expert' || error) return <Unauthorized />


  return (
    <Layout>
      <div className="w-full text-black dark:text-white flex items-center justify-between">
        <span className="text-2xl font-bold">Edit Activity</span>
      </div>
      <div className="w-full my-4 rounded-xl bg-gray-50 p-4 text-black">
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Title</label>
          <input
            type="text"
            className="form-input px-4 py-3 w-full rounded-lg"
            value={formData.title}
            onChange={(event) => {
              setFormData({...formData, title: event.target.value})
            }}
          />
        </div>
        <div className="w-full mb-3">
          <label className="font-medium leading-none">Description</label>
          <textarea 
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, description: event.target.value})
            }}
            value={formData.description}
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Skill</label>
          <select 
            className="form-input px-4 py-3 w-full rounded-lg"
            value={formData.skill}
            onChange={(event) => {
              setFormData({...formData, skill: event.target.value})
            }}
          >
            { skills.map((val, idx) => (
                <option key={idx} value={val.id} >{val.skill_name}</option>
              ))
            }
          </select>
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none block mb-1">Start Date</label>
          <DatePicker 
            className="form-input px-4 py-3 w-full rounded-lg"
            selected={startDate} 
            onChange={(date) => {
              setStartDate(date)
              setFormData({...formData, startdate: dayjs(date).format('YYYY-MM-DD')})
            }} 
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none block mb-1">End Date</label>
          <DatePicker 
            className="form-input px-4 py-3 w-full rounded-lg"
            selected={endDate} 
            onChange={(date) => {
              setEndDate(date)
              setFormData({...formData, enddate: dayjs(date).format('YYYY-MM-DD')})
            }} 
            minDate={startDate}
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Participants</label>
          <AsyncSelect
            cacheOptions
            instanceId="select-value"
            loadOptions={loadOptions}
            defaultValue={activity.participants.map(item => ({value: item.id, label: item.name}))}
            isMulti={true}
            onChange={(e) => setFormData({...formData, participants: e.map(item => item.value)})}
          />
        </div>
        
        <div className="w-full mt-3 flex items-center justify-end">
          <button onClick={handleSubmit} className="bg-green-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2">
            Save
            <CheckCircleIcon className="ml-2 h-6 w-6" aria-hidden="true"  />
          </button>
        </div>
      </div>
    </Layout>
  )
}

export default CreateNewActivity;