import { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { CheckCircleIcon } from "@heroicons/react/24/outline";
import DatePicker from "react-datepicker";
import AsyncSelect from 'react-select/async';
import axios from "axios";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import dayjs from 'dayjs';

import Layout from "@/components/Layout";

import "react-datepicker/dist/react-datepicker.css";
import Unauthorized from "@/components/Unauthorized";

const CreateNewActivity = () => {

  const [token, setToken] = useState(null);
  const [skills, setSkills] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [formData, setFormData] = useState({
    title: null,
    description: null,
    skill: null,
    startdate: dayjs().format('YYYY-MM-DD'),
    enddate: dayjs().format('YYYY-MM-DD'),
    participants: []
  })
  const { data: session } = useSession();
  const swal = withReactContent(Swal);
  const router = useRouter();

  useEffect(() => {
    if(session) setToken(session.accessToken)
  }, [session]);

  useEffect(() => {
    if(token)
      fetchSkills()
  }, [token])

  const fetchSkills =  () => {
    axios({
      method: 'get',
      url: `${process.env.NEXT_PUBLIC_API_URL}/skills?allData=true`,
      headers: {
        "Authorization": `Bearer ${token}`,
      }
    })
    .then((resp) => {
      const data = resp.data;
      setSkills(data.data);
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch data</p>,
        icon: 'error'
      })
    })
  }

  const fetchUser = async (q) => {
    var userData = [];

    if(session){

      await axios({
        method: 'get',
        url: `${process.env.NEXT_PUBLIC_API_URL}/users?perPage=10&q=${q}`,
        headers: {
          "Authorization": `Bearer ${token}`,
        }
      })
      .then((resp) => {
        const data = resp.data.data;

        data.map((val) => {
          userData.push({
            value: val.id,
            label: `${val.name}`
          })
        })
      })
      .catch((err) => {
        console.log(err)
        swal.fire({
          title: <p>Failed to fetch data</p>,
          icon: 'error'
        })
      })
    }
    return userData;
  }

  const loadOptions = (inputValue, callback) => {
    setTimeout(async () => {
      const data = await fetchUser(inputValue);
      callback(data)
    }, 1000)
  }


  const handleSubmit = () => {
    axios({
      method: 'post',
      url: `${process.env.NEXT_PUBLIC_API_URL}/activity`,
      headers: {
        "Authorization": `Bearer ${token}`,
      },
      data: formData
    })
    .then((resp) => {
      swal.fire({
        title: <p>Data Submitted</p>,
        icon: 'success'
      }).then(() => {
        router.push('/dashboard')
      })
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch data</p>,
        icon: 'error'
      })
    })
  }

  if(session?.profile != 'expert') return <Unauthorized />


  return (
    <Layout>
      <div className="w-full text-black dark:text-white flex items-center justify-between">
        <span className="text-2xl font-bold">Create New Activity</span>
      </div>
      <div className="w-full my-4 rounded-xl bg-gray-50 p-4 text-black">
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Title</label>
          <input
            type="text"
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, title: event.target.value})
            }}
          />
        </div>
        <div className="w-full mb-3">
          <label className="font-medium leading-none">Description</label>
          <textarea 
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, description: event.target.value})
            }}
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Skill</label>
          <select 
            className="form-input px-4 py-3 w-full rounded-lg"
            onChange={(event) => {
              setFormData({...formData, skill: event.target.value})
            }}
          >
            { skills.map((val, idx) => (
                <option key={idx} value={val.id}>{val.skill_name}</option>
              ))
            }
          </select>
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none block mb-1">Start Date</label>
          <DatePicker 
            className="form-input px-4 py-3 w-full rounded-lg"
            selected={startDate} 
            onChange={(date) => {
              setStartDate(date)
              setFormData({...formData, startdate: dayjs(date).format('YYYY-MM-DD')})
            }} 
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none block mb-1">End Date</label>
          <DatePicker 
            className="form-input px-4 py-3 w-full rounded-lg"
            selected={endDate} 
            onChange={(date) => {
              setEndDate(date)
              setFormData({...formData, enddate: dayjs(date).format('YYYY-MM-DD')})
            }} 
            minDate={startDate}
          />
        </div>
        <div className="w-full lg:w-1/3 mb-3">
          <label className="font-medium leading-none">Participants</label>
          <AsyncSelect
            cacheOptions
            instanceId="select-value"
            loadOptions={loadOptions}
            isMulti={true}
            onChange={(e) => setFormData({...formData, participants: e.map(item => item.value)})}
          />
        </div>
        
        <div className="w-full mt-3 flex items-center justify-end">
          <button onClick={handleSubmit} className="bg-green-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2">
            Save
            <CheckCircleIcon className="ml-2 h-6 w-6" aria-hidden="true"  />
          </button>
        </div>
      </div>
    </Layout>
  )
}

export default CreateNewActivity;