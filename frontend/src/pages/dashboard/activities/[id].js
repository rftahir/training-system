import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import dayjs from "dayjs";

import Layout from "@/components/Layout";
import { PencilSquareIcon, TrashIcon } from "@heroicons/react/24/outline";

const DetailActivity = () => {
  const [activity, setActivity] = useState({
    title: null,
    description: null,
    skill: null,
    startdate: dayjs().format("YYYY-MM-DD"),
    enddate: dayjs().format("YYYY-MM-DD"),
    participants: [],
  });
  const [token, setToken] = useState(null);
  const { data: session } = useSession();
  const swal = withReactContent(Swal);
  const router = useRouter();

  useEffect(() => {
    if (session) setToken(session.accessToken);
  }, [session]);

  useEffect(() => {
    if (token) fetchData();
  }, [token]);

  const fetchData = () => {
    axios({
      method: "get",
      url: `${process.env.NEXT_PUBLIC_API_URL}/activity/${router.query.id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((resp) => {
      const data = resp.data;
      setActivity(data.data);
    })
    .catch(() => {
      swal.fire({
        title: <p>Failed to fetch data</p>,
        icon: "error",
      });
    });
  };

  const handleDelete = () => {
    swal.fire({
      title: <p>Are you sure want to delete this data ?</p>,
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    }).then((value) => {
      if(value){
        axios({
          method: "delete",
          url: `${process.env.NEXT_PUBLIC_API_URL}/activity/${router.query.id}`,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((resp) => {
          swal.fire('Data has been deleted', '', 'success')
              .then(() => router.push('/dashboard'));
        })
        .catch(() => {
          swal.fire({
            title: <p>Failed to delete data</p>,
            icon: "error",
          });
        });
      }
    })
  }

  return (
    <Layout>
      <div className="w-full text-black dark:text-white flex items-center justify-between">
        <span className="text-2xl font-bold">{activity.title}</span>
        { session?.profile == 'expert' &&
          <div>
            <button 
              onClick={handleDelete}
              className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2"
            >
              <TrashIcon className="mr-2 h-6 w-6" aria-hidden="true"  />
              Delete Activity
            </button>
            <button 
              onClick={() => router.push(`/dashboard/activities/${router.query.id}/edit`)}
              className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded inline-flex items-center mx-2"
            >
              <PencilSquareIcon className="mr-2 h-6 w-6" aria-hidden="true"  />
              Edit Activity
            </button>
          </div>
        }
      </div>
      <div className="w-full my-4 rounded-xl bg-gray-50 p-4 text-black">
        <div className="w-full mb-4">
          <p className="text-lg font-bold">Description :</p>
          <p>{activity.description}</p>
        </div>
        <div className="w-full mb-4">
          <p className="text-lg font-bold">Skill: </p>
          <p>{activity.skill_name}</p>
        </div>
        <div className="w-full mb-4">
          <p className="text-lg font-bold">Date: </p>
          <p>
            {dayjs(activity.startdate).format("MMMM D, YYYY")} -{" "}
            {dayjs(activity.enddate).format("MMMM D, YYYY")}
          </p>
        </div>
        <div className="w-full mb-4">
          <p className="text-lg font-bold">Participants: </p>
          <table className="table-auto w-full">
            <thead className="border-b">
              <tr className="bg-gray-100">
                <th className="text-left p-4 font-medium">Name</th>
                <th className="text-left p-4 font-medium">Skill</th>
                <th className="text-left p-4 font-medium">Profile</th>
              </tr>
            </thead>
            <tbody>
              { activity.participants.map((val, idx) => (
                  <tr className="border-b hover:bg-gray-50" key={`table-${val.id}`}>
                    <td className="p-4">{val.name}</td>
                    <td className="p-4">{val.skill}</td>
                    <td className="p-4">{val.profile}</td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    </Layout>
  );
};

export default DetailActivity;
