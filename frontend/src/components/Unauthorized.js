import Layout from "./Layout";

const Unauthorized = () => {
  return (
    <Layout>
      <div className="flex flex-col items-center justify-center">
        <p className="text-9xl">401</p>
        <p className="text-2xl">You are not authorized to access this page.</p>
      </div>
    </Layout>
  )
}

export default Unauthorized;