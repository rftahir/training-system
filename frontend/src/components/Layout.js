import Navbar from "./Navbar";

const Layout = ({children}) => {
 return (
    <div className="min-h-screen bg-gray-300 dark:bg-gray-800 w-full">
      <Navbar />
      <div className="container mx-auto my-3 p-4">
        { children }
      </div>
    </div>
 )
}

export default Layout;