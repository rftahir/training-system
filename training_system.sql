--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Debian 15.3-1.pgdg120+1)
-- Dumped by pg_dump version 15.3 (Debian 15.3-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: activities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activities (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    skill uuid NOT NULL,
    title character varying NOT NULL,
    description text NOT NULL,
    startdate date NOT NULL,
    enddate date NOT NULL
);


ALTER TABLE public.activities OWNER TO postgres;

--
-- Name: participants; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.participants (
    activity_id uuid NOT NULL,
    user_id uuid NOT NULL
);


ALTER TABLE public.participants OWNER TO postgres;

--
-- Name: skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.skills (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    skill_name character varying NOT NULL
);


ALTER TABLE public.skills OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    username character varying NOT NULL,
    password character varying NOT NULL,
    skill uuid,
    profile character varying NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: activities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.activities (id, skill, title, description, startdate, enddate) FROM stdin;
994bcff5-fa93-426c-bbf8-454ee70fdf75	0dbddd37-f13c-45c5-8357-0e5ba5015a3c	Java Tutorial for Complete Beginners	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-11	2023-08-29
a4745808-2f2c-4c30-aace-e75188f32f87	ad0460ad-6e36-4f8a-8027-129bbc7874d5	The Complete Python Bootcamp From Zero to Hero in Python	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-01	2023-08-20
1ade2dd0-0950-4083-82c5-91d3befc1c80	823337b5-c4c1-4ce4-8bd3-7747dcd082b7	Microsoft Excel – Excel from Beginner to Advanced	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-12	2023-08-30
c924f36b-5e0c-4151-815a-cf47db1f8221	f69a9815-40a1-4f94-92e0-cffc4b1289bd	Automate the Boring Stuff with Python Programming	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-09	2023-08-18
7424dde8-9d59-492d-ae96-8d2f33732fdb	f69a9815-40a1-4f94-92e0-cffc4b1289bd	The Complete 2023 Web Development Bootcamp	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-09	2023-08-17
7ae753ec-27a0-4199-ac26-4bac9cb33b25	98e7a219-14ed-4364-9112-145283013bdc	The Web Developer Bootcamp 2023	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-02	2023-08-25
ce51ccf0-a8d8-4e34-bfed-e9e73bc27337	0effd60e-c257-4eca-be81-b6476313f288	Introduction To Python Programming	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-11	2023-08-23
d0d3816c-21ee-4c1f-a6a7-28af0ba918a5	0dbddd37-f13c-45c5-8357-0e5ba5015a3c	100 Days of Code: The Complete Python Pro Bootcamp for 2023	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-02	2023-08-28
1d13c41a-974a-4362-8d27-9b342d4e4950	0b9d8a44-52b1-4b1d-b1be-9634a483cd19	Java Programming Masterclass updated to Java 17	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-12	2023-08-23
9d43fb35-9ad0-4ebc-bae4-07d240b543c8	80efaf7c-0a41-4e78-bfa4-0ce52821d31c	The Complete JavaScript Course 2023: From Zero to Expert!	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-10	2023-08-19
3774a64e-e2ca-42b1-9c49-c0022e568b35	0daf2673-6a0d-4d91-916d-4c8774e0fcac	The Complete Digital Marketing Course – 12 Courses in 1	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-09	2023-08-17
aaa1742f-beb7-4ba1-955e-a6bd2db919b6	0daf2673-6a0d-4d91-916d-4c8774e0fcac	Ultimate AWS Certified Solutions Architect Associate SAA-C03	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-12	2023-08-21
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	0dbddd37-f13c-45c5-8357-0e5ba5015a3c	Angular – The Complete Guide (2023 Edition)	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-04	2023-08-23
6e0309f5-9f4f-4967-b8ba-5aec9852fd01	0dbddd37-f13c-45c5-8357-0e5ba5015a3c	The Complete SQL Bootcamp: Go from Zero to Hero	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-06	2023-08-28
f45876a9-244f-409b-86c0-0218ffbcdcfe	98e7a219-14ed-4364-9112-145283013bdc	Web Design for Web Developers: Build Beautiful Websites!	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-10	2023-08-28
6ce3a9ff-51de-4cc1-b3d1-5f09de72a820	f69a9815-40a1-4f94-92e0-cffc4b1289bd	Ultimate AWS Certified Cloud Practitioner – 2022	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-12	2023-08-25
43c3aa5a-b09b-4fe0-802b-9006eb95c9c9	2271b31d-c5b5-4d56-9972-8c756db1748a	Python for Data Science and Machine Learning Bootcamp	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-07	2023-08-17
78909bdd-93d3-4a56-8782-4f767344185b	18e86ce4-8c3d-4021-90c6-2967674694b5	C++ Tutorial for Complete Beginners	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-09	2023-08-24
97a9b02a-de83-4d00-8c85-2b56ff76d0a9	0dbddd37-f13c-45c5-8357-0e5ba5015a3c	Become an Android Developer from Scratch	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-12	2023-08-26
2f89d604-4550-4897-a39b-79755d7ad631	0b9d8a44-52b1-4b1d-b1be-9634a483cd19	AWS Certified Solutions Architect – Associate 2020	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-09	2023-08-29
45e92060-70c8-49ee-855c-eea899134624	ad0460ad-6e36-4f8a-8027-129bbc7874d5	The Complete Python Programming Course: Beginner to Advanced	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-09	2023-08-26
82952a02-0262-4255-9ab2-4c3cf60f611f	0daf2673-6a0d-4d91-916d-4c8774e0fcac	The Data Science Course 2023: Complete Data Science Bootcamp	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus ornare urna, ut congue nunc auctor in. Vivamus ultricies dui augue, ut malesuada ipsum iaculis at. Praesent cursus eros sit amet massa tristique efficitur. Mauris sollicitudin interdum nibh, ut aliquet dolor finibus ac.	2023-08-14	2023-08-21
\.


--
-- Data for Name: participants; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.participants (activity_id, user_id) FROM stdin;
994bcff5-fa93-426c-bbf8-454ee70fdf75	1539b365-835e-4b35-8576-ec71c5e51f3d
9d43fb35-9ad0-4ebc-bae4-07d240b543c8	d584565c-c607-4781-85e9-f7cf7f9b339f
c924f36b-5e0c-4151-815a-cf47db1f8221	25c7d6f7-3afe-42d8-8afc-6fb4a013ccc7
7ae753ec-27a0-4199-ac26-4bac9cb33b25	d584565c-c607-4781-85e9-f7cf7f9b339f
6ce3a9ff-51de-4cc1-b3d1-5f09de72a820	d584565c-c607-4781-85e9-f7cf7f9b339f
a4745808-2f2c-4c30-aace-e75188f32f87	17f1f2cc-ee35-4324-bbe0-8a1d0215afb7
97a9b02a-de83-4d00-8c85-2b56ff76d0a9	9f72faeb-745d-4583-a37e-832131bea1a0
ce51ccf0-a8d8-4e34-bfed-e9e73bc27337	d584565c-c607-4781-85e9-f7cf7f9b339f
97a9b02a-de83-4d00-8c85-2b56ff76d0a9	a19fe69e-72b4-4d44-9e33-fe750557652d
d0d3816c-21ee-4c1f-a6a7-28af0ba918a5	9f72faeb-745d-4583-a37e-832131bea1a0
45e92060-70c8-49ee-855c-eea899134624	dd9f0c93-98f8-4bd4-8f32-07f0fc67eae0
43c3aa5a-b09b-4fe0-802b-9006eb95c9c9	5965339b-855f-4a40-bed0-7ccdcd5dcb3b
3774a64e-e2ca-42b1-9c49-c0022e568b35	a8473cf0-a662-4c82-ae46-0da4344295d6
7ae753ec-27a0-4199-ac26-4bac9cb33b25	563f1d45-1ffb-4041-b094-a82717b4bbbc
c924f36b-5e0c-4151-815a-cf47db1f8221	564f9098-e811-4342-8aa9-dd548402371c
ce51ccf0-a8d8-4e34-bfed-e9e73bc27337	dd9f0c93-98f8-4bd4-8f32-07f0fc67eae0
7ae753ec-27a0-4199-ac26-4bac9cb33b25	a19fe69e-72b4-4d44-9e33-fe750557652d
82952a02-0262-4255-9ab2-4c3cf60f611f	17f1f2cc-ee35-4324-bbe0-8a1d0215afb7
6e0309f5-9f4f-4967-b8ba-5aec9852fd01	836eeb99-40d6-4f6c-be17-75eaab82f238
9d43fb35-9ad0-4ebc-bae4-07d240b543c8	1539b365-835e-4b35-8576-ec71c5e51f3d
2f89d604-4550-4897-a39b-79755d7ad631	72544fb1-0701-468c-a428-9a45724a4ea3
6ce3a9ff-51de-4cc1-b3d1-5f09de72a820	6dfe6711-1058-4745-8557-e7c48d60cd4d
ce51ccf0-a8d8-4e34-bfed-e9e73bc27337	5965339b-855f-4a40-bed0-7ccdcd5dcb3b
1d13c41a-974a-4362-8d27-9b342d4e4950	1539b365-835e-4b35-8576-ec71c5e51f3d
994bcff5-fa93-426c-bbf8-454ee70fdf75	676f6631-c982-485c-9f73-5eacdadf6e39
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	dbca0cfd-1c56-4be6-8e47-beae984d385b
c924f36b-5e0c-4151-815a-cf47db1f8221	551eb930-0e5d-43ae-a30c-3f4b6596e9ab
9d43fb35-9ad0-4ebc-bae4-07d240b543c8	07bc4986-b238-4501-a2a7-67b034e3cce5
6e0309f5-9f4f-4967-b8ba-5aec9852fd01	6dfe6711-1058-4745-8557-e7c48d60cd4d
7424dde8-9d59-492d-ae96-8d2f33732fdb	6d16d86a-2380-41af-8d4a-5132364b6bdc
d0d3816c-21ee-4c1f-a6a7-28af0ba918a5	bc087e2d-1f55-4907-ba52-bf03b87c6b11
7ae753ec-27a0-4199-ac26-4bac9cb33b25	07bc4986-b238-4501-a2a7-67b034e3cce5
6ce3a9ff-51de-4cc1-b3d1-5f09de72a820	676f6631-c982-485c-9f73-5eacdadf6e39
7ae753ec-27a0-4199-ac26-4bac9cb33b25	25c7d6f7-3afe-42d8-8afc-6fb4a013ccc7
c924f36b-5e0c-4151-815a-cf47db1f8221	ff35a381-20cc-4ce6-b524-e9142c68ac02
6e0309f5-9f4f-4967-b8ba-5aec9852fd01	514b147a-40d3-4c7a-b437-5816b8de984f
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	514b147a-40d3-4c7a-b437-5816b8de984f
a4745808-2f2c-4c30-aace-e75188f32f87	676f6631-c982-485c-9f73-5eacdadf6e39
82952a02-0262-4255-9ab2-4c3cf60f611f	ce789288-a8f4-4e79-ae4c-7a59823835ad
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	fc9e735f-4fae-4231-9073-f0d4929e84c5
1d13c41a-974a-4362-8d27-9b342d4e4950	a19fe69e-72b4-4d44-9e33-fe750557652d
f45876a9-244f-409b-86c0-0218ffbcdcfe	836eeb99-40d6-4f6c-be17-75eaab82f238
aaa1742f-beb7-4ba1-955e-a6bd2db919b6	25c7d6f7-3afe-42d8-8afc-6fb4a013ccc7
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	b067cbe0-f712-4c9d-bbd1-f939d5019e3b
6e0309f5-9f4f-4967-b8ba-5aec9852fd01	564f9098-e811-4342-8aa9-dd548402371c
82952a02-0262-4255-9ab2-4c3cf60f611f	bc087e2d-1f55-4907-ba52-bf03b87c6b11
c924f36b-5e0c-4151-815a-cf47db1f8221	0e0adc16-2505-4f6a-9644-6b78f68cd697
2f89d604-4550-4897-a39b-79755d7ad631	5965339b-855f-4a40-bed0-7ccdcd5dcb3b
7424dde8-9d59-492d-ae96-8d2f33732fdb	25c7d6f7-3afe-42d8-8afc-6fb4a013ccc7
7424dde8-9d59-492d-ae96-8d2f33732fdb	dd9f0c93-98f8-4bd4-8f32-07f0fc67eae0
d0d3816c-21ee-4c1f-a6a7-28af0ba918a5	ff35a381-20cc-4ce6-b524-e9142c68ac02
c924f36b-5e0c-4151-815a-cf47db1f8221	a8473cf0-a662-4c82-ae46-0da4344295d6
78909bdd-93d3-4a56-8782-4f767344185b	b067cbe0-f712-4c9d-bbd1-f939d5019e3b
1ade2dd0-0950-4083-82c5-91d3befc1c80	ff35a381-20cc-4ce6-b524-e9142c68ac02
45e92060-70c8-49ee-855c-eea899134624	bc087e2d-1f55-4907-ba52-bf03b87c6b11
994bcff5-fa93-426c-bbf8-454ee70fdf75	5965339b-855f-4a40-bed0-7ccdcd5dcb3b
d0d3816c-21ee-4c1f-a6a7-28af0ba918a5	72544fb1-0701-468c-a428-9a45724a4ea3
f45876a9-244f-409b-86c0-0218ffbcdcfe	ec392841-13bf-409c-888c-3c50d6c2f60d
3774a64e-e2ca-42b1-9c49-c0022e568b35	9f72faeb-745d-4583-a37e-832131bea1a0
82952a02-0262-4255-9ab2-4c3cf60f611f	b067cbe0-f712-4c9d-bbd1-f939d5019e3b
a4745808-2f2c-4c30-aace-e75188f32f87	b067cbe0-f712-4c9d-bbd1-f939d5019e3b
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	07bc4986-b238-4501-a2a7-67b034e3cce5
aaa1742f-beb7-4ba1-955e-a6bd2db919b6	551eb930-0e5d-43ae-a30c-3f4b6596e9ab
6e0309f5-9f4f-4967-b8ba-5aec9852fd01	b067cbe0-f712-4c9d-bbd1-f939d5019e3b
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	bc087e2d-1f55-4907-ba52-bf03b87c6b11
994bcff5-fa93-426c-bbf8-454ee70fdf75	ec392841-13bf-409c-888c-3c50d6c2f60d
3774a64e-e2ca-42b1-9c49-c0022e568b35	551eb930-0e5d-43ae-a30c-3f4b6596e9ab
45e92060-70c8-49ee-855c-eea899134624	17f1f2cc-ee35-4324-bbe0-8a1d0215afb7
7424dde8-9d59-492d-ae96-8d2f33732fdb	fc9e735f-4fae-4231-9073-f0d4929e84c5
2f89d604-4550-4897-a39b-79755d7ad631	dd9f0c93-98f8-4bd4-8f32-07f0fc67eae0
45e92060-70c8-49ee-855c-eea899134624	d584565c-c607-4781-85e9-f7cf7f9b339f
3dcba174-2fb1-4b08-9b48-7a530b3d42a7	a8473cf0-a662-4c82-ae46-0da4344295d6
7ae753ec-27a0-4199-ac26-4bac9cb33b25	6d16d86a-2380-41af-8d4a-5132364b6bdc
7424dde8-9d59-492d-ae96-8d2f33732fdb	836eeb99-40d6-4f6c-be17-75eaab82f238
\.


--
-- Data for Name: skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.skills (id, skill_name) FROM stdin;
0daf2673-6a0d-4d91-916d-4c8774e0fcac	HTML/CSS
ad0460ad-6e36-4f8a-8027-129bbc7874d5	JavaScript
fc6333cd-12c2-478b-8b37-8ba1b64a669b	Responsive Design
0ae9b71c-522d-431d-b095-57d626895b16	UI/UX Design
98e7a219-14ed-4364-9112-145283013bdc	Git
88c2998f-60b7-4fe4-b771-bc5b361e4dc2	Frontend Frameworks
9fd84d5c-ae9f-42ef-aa6b-4080122aa47c	Backend Programming
2271b31d-c5b5-4d56-9972-8c756db1748a	API Development
80efaf7c-0a41-4e78-bfa4-0ce52821d31c	Database Management
0dbddd37-f13c-45c5-8357-0e5ba5015a3c	Server Administration
f69a9815-40a1-4f94-92e0-cffc4b1289bd	Cloud Computing
c5cc7a75-9c0a-4655-95db-41286e66d09e	DevOps
0effd60e-c257-4eca-be81-b6476313f288	Testing and Debugging
aab22f3c-3353-41eb-862d-91d98d2789dd	Security
18e86ce4-8c3d-4021-90c6-2967674694b5	Networking
49edafe1-7023-43de-b9f4-f0a6315795c9	Agile Methodologies
823337b5-c4c1-4ce4-8bd3-7747dcd082b7	Containerization
4dbf5fd0-5765-45b2-b16a-8ad20b324165	Mobile Development
6d0f7354-e6c2-4421-a021-28736ae76eeb	Data Analysis
0b9d8a44-52b1-4b1d-b1be-9634a483cd19	Problem-Solving
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, username, password, skill, profile) FROM stdin;
72544fb1-0701-468c-a428-9a45724a4ea3	John Doe	johndoe@example.com	rftahir123	$2b$10$pWPTZm4sOZF/8AQJ8JoUYOBUN.0f2pFGKJX8gtY0WoxjtPFHPJqr6	\N	expert
563f1d45-1ffb-4041-b094-a82717b4bbbc	Jane Smith	jane.smith@example.com	jsmith21	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	0daf2673-6a0d-4d91-916d-4c8774e0fcac	expert
9f72faeb-745d-4583-a37e-832131bea1a0	Michael Johnson	michael.johnson@example.com	mjohnson89	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	ad0460ad-6e36-4f8a-8027-129bbc7874d5	trainer
6dfe6711-1058-4745-8557-e7c48d60cd4d	Emily Davis	emily.davis@example.com	edavis456	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	fc6333cd-12c2-478b-8b37-8ba1b64a669b	competitor
ce789288-a8f4-4e79-ae4c-7a59823835ad	David Lee	david.lee@example.com	dlee007	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	\N	board
1539b365-835e-4b35-8576-ec71c5e51f3d	Sarah Wilson	sarah.wilson@example.com	swilson22	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	0ae9b71c-522d-431d-b095-57d626895b16	expert
564f9098-e811-4342-8aa9-dd548402371c	Robert Thompson	robert.thompson@example.com	rthompson3	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	98e7a219-14ed-4364-9112-145283013bdc	trainer
25c7d6f7-3afe-42d8-8afc-6fb4a013ccc7	Amy Roberts	amy.roberts@example.com	aroberts10	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	88c2998f-60b7-4fe4-b771-bc5b361e4dc2	competitor
6d16d86a-2380-41af-8d4a-5132364b6bdc	Daniel Clark	daniel.clark@example.com	dclark28	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	\N	board
07bc4986-b238-4501-a2a7-67b034e3cce5	Jessica Brown	jessica.brown@example.com	jbrown91	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	9fd84d5c-ae9f-42ef-aa6b-4080122aa47c	expert
17f1f2cc-ee35-4324-bbe0-8a1d0215afb7	Andrew Walker	andrew.walker@example.com	awalker75	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	2271b31d-c5b5-4d56-9972-8c756db1748a	trainer
dbca0cfd-1c56-4be6-8e47-beae984d385b	Olivia Harris	olivia.harris@example.com	oharris44	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	80efaf7c-0a41-4e78-bfa4-0ce52821d31c	competitor
dd9f0c93-98f8-4bd4-8f32-07f0fc67eae0	Christopher Martinez	christopher.martinez@example.com	cmartinez16	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	\N	board
551eb930-0e5d-43ae-a30c-3f4b6596e9ab	Samantha Green	samantha.green@example.com	sgreen33	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	0dbddd37-f13c-45c5-8357-0e5ba5015a3c	expert
aceaf9b7-d2bc-4bfe-9bf0-d2a52e70ba09	Matthew Rodriguez	matthew.rodriguez@example.com	mrodriguez2	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	f69a9815-40a1-4f94-92e0-cffc4b1289bd	trainer
b067cbe0-f712-4c9d-bbd1-f939d5019e3b	Ava King	ava.king@example.com	aking87	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	c5cc7a75-9c0a-4655-95db-41286e66d09e	competitor
0e0adc16-2505-4f6a-9644-6b78f68cd697	William Campbell	william.campbell@example.com	wcampbell19	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	\N	board
fc9e735f-4fae-4231-9073-f0d4929e84c5	Madison Young	madison.young@example.com	myoung23	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	0effd60e-c257-4eca-be81-b6476313f288	expert
a8473cf0-a662-4c82-ae46-0da4344295d6	Joseph Turner	joseph.turner@example.com	jturner85	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	aab22f3c-3353-41eb-862d-91d98d2789dd	trainer
5965339b-855f-4a40-bed0-7ccdcd5dcb3b	Chloe Walker	chloe.walker@example.com	cwalker42	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	18e86ce4-8c3d-4021-90c6-2967674694b5	competitor
ff35a381-20cc-4ce6-b524-e9142c68ac02	Ethan Lewis	ethan.lewis@example.com	elewis61	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	\N	board
836eeb99-40d6-4f6c-be17-75eaab82f238	Grace Hall	grace.hall@example.com	ghall7	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	49edafe1-7023-43de-b9f4-f0a6315795c9	expert
a19fe69e-72b4-4d44-9e33-fe750557652d	Benjamin Hill	benjamin.hill@example.com	bhill94	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	823337b5-c4c1-4ce4-8bd3-7747dcd082b7	trainer
d584565c-c607-4781-85e9-f7cf7f9b339f	Lily Phillips	lily.phillips@example.com	lphillips39	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	4dbf5fd0-5765-45b2-b16a-8ad20b324165	competitor
ec392841-13bf-409c-888c-3c50d6c2f60d	Samuel Ross	samuel.ross@example.com	sross58	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	\N	board
bc087e2d-1f55-4907-ba52-bf03b87c6b11	Avery Carter	avery.carter@example.com	acarter8	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	6d0f7354-e6c2-4421-a021-28736ae76eeb	expert
676f6631-c982-485c-9f73-5eacdadf6e39	Victoria Allen	victoria.allen@example.com	vallen76	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	0b9d8a44-52b1-4b1d-b1be-9634a483cd19	trainer
514b147a-40d3-4c7a-b437-5816b8de984f	John Doe	john.doe@example.com	john_doe	$2b$10$VGlA1YbMIuXSkhHCZl.AZuqih.Z3oXyJOz1gqUSw6ILUFFQCehj5y	\N	board
03264d36-9283-47bf-8b62-de4b53964cb6	John Marston	johnmarston@email.com	johnmarston	$2b$10$RilhAQxoUg/2m0QKACPGF.VJ/qBnrcHaWT/GV0evs/1nG234A95/W	9fd84d5c-ae9f-42ef-aa6b-4080122aa47c	expert
\.


--
-- Name: activities activities_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activities
    ADD CONSTRAINT activities_pk PRIMARY KEY (id);


--
-- Name: participants participants_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participants
    ADD CONSTRAINT participants_unique UNIQUE (activity_id, user_id);


--
-- Name: skills skills_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.skills
    ADD CONSTRAINT skills_pk PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);


--
-- Name: users users_username_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_unique UNIQUE (username);


--
-- Name: activities activities_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activities
    ADD CONSTRAINT activities_fk FOREIGN KEY (skill) REFERENCES public.skills(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: participants participant_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participants
    ADD CONSTRAINT participant_fk FOREIGN KEY (activity_id) REFERENCES public.activities(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: participants participant_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participants
    ADD CONSTRAINT participant_fk_1 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users users_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_fk FOREIGN KEY (skill) REFERENCES public.skills(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

