function loadEnvironmentVariable(keyname) {
    const envVar = process.env[keyname];

    if (!envVar) {
        throw new Error(`Configuration must include ${keyname}`)
    }

    return envVar;
}

module.exports = {
    appHost:loadEnvironmentVariable('APP_HOST'),
    secretKey: loadEnvironmentVariable('SECRET_KEY'),
    dbHost:loadEnvironmentVariable('DB_HOST'),
    dbPort:loadEnvironmentVariable('DB_PORT'),
    dbName:loadEnvironmentVariable('DB_NAME'),
    dbUsername:loadEnvironmentVariable('DB_USER'),
    dbPassword:loadEnvironmentVariable('DB_PASS'),
    redisHost:loadEnvironmentVariable('REDIS_HOST'),
    redisPort:loadEnvironmentVariable('REDIS_PORT'),
    redisPassword:loadEnvironmentVariable('REDIS_PASS'),

}