const BaseFilterSchema = {
  type: 'object',
  properties: {
    perPage: { type: 'number', default: 10, nullable: true },
    currentPage: { type: 'number', default: 1, nullable: true },
    order: { type: 'string', nullable: true},
    orderType: {
      anyOf: [
        {type: 'string', const: 'asc'},
        {type: 'string', const: 'desc'},
      ]
    },
    q: {type: 'string', nullable: true},
    allData: {type: 'boolean', nullable: true}
  }
}

module.exports = {
  BaseFilterSchema
}