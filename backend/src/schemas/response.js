const createBaseResponseSchema = (data) => (
  {
    allOf: [
        {
          type: 'object',
          properties: {
            message: { type: 'string' }
          }
        }, 
        {
          type: 'object',
          properties: {
            data
          }
        }
      ]
  }
)

const createBaseResponseDataPagination = (data) => (
  {
    allOf: [
      createBaseResponseSchema(data),
      {
        type: 'object',
        properties: {
          total: {type: 'number', nullable: true},
          lastPage: {type: 'number', nullable: true},
          currentPage: {type: 'number', nullable: true},
          perPage: {type: 'number', nullable: true},
          from: {type: 'number', nullable: true},
          to: {type: 'number', nullable: true},
        }
      }
    ]
  }
)

const ErrorResponseSchema = {
  allOf: [
    {
      type: 'object',
      properties: {
        statusCode: { type: 'number' }
      }
    },
    {
      type: 'object',
      properties: {
        error: { type: 'string' }
      }
    },
    {
      type: 'object',
      properties: {
        message: { type: 'string' }
      }
    }
  ]
}

module.exports = {
  createBaseResponseSchema,
  ErrorResponseSchema,
  createBaseResponseDataPagination
}

