'use strict'

const fp = require('fastify-plugin');
const { getRedis } = require('../helpers');
const config = require('../config/config');

module.exports = fp(function (fastify, opts, done) {
    fastify.register(require('@fastify/jwt'), {
        secret: config.secretKey,
        trusted: validateToken,
        formatUser: function (user) {
          return {
            ...user,
            role: [user.profile]
          }
        },
    })

    fastify.decorate("authenticate", async function (request, reply) {
        try {
          await request.jwtVerify()
        } catch (err) {
          reply.send(err)
        }
    })


    async function validateToken(request, decodedToken){
      let blacklist = await getRedis(fastify, 'blacklist');
      const auth = request.headers.authorization;
      const token = auth.split(' ')[1];
      
      if(blacklist){
        blacklist = JSON.parse(blacklist);
      }else{
        blacklist = [];
      }
      
      return !blacklist.includes(token)
    }

    done()
})