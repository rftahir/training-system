'use strict'

const fp = require('fastify-plugin');
const config = require('../config/config');
const knexJS = require('fastify-knexjs');
const { attachPaginate } = require('knex-paginate');

module.exports = fp(function (fastify, opts, done) {
    fastify.register(
      knexJS,
      {
        client: 'pg',
        connection: {
          host: config.dbHost,
          port: config.dbPort,
          user: config.dbUsername,
          password: config.dbPassword,
          database: config.dbName,
        },
      },
      (err) => {
        console.error(err);
        process.exit(169);
      }
    );

    attachPaginate();

    done()
})