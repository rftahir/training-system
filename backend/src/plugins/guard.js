'use strict'

const fp = require('fastify-plugin');
const schema =

module.exports = fp(function (fastify, opts, done) {
    fastify.register(
      require('fastify-guard'),
      {
        errorHandler: (result, req, reply) => {
          return reply.status(401).send({
            statusCode: 401,
            error: 'Unauthorized',
            message: 'You are not allowed to access this page',
          })
        }
      }
    )

    done()
})