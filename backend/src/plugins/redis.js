'use strict'

const fp = require('fastify-plugin');
const config = require('../config/config');
const redis = require('@fastify/redis');

module.exports = fp(function (fastify, opts, done) {
    fastify.register(redis, { 
      host: config.redisHost, 
      password: config.redisPassword,
      port: config.redisPort
    });

    done()
})