'use strict'

const fp = require('fastify-plugin');
const cors = require('@fastify/cors')

module.exports = fp(async function (fastify, opts, done) {
    
    
    await fastify.register(cors, { 
      origin: 'http://localhost:3000'
    })

    done()
})