const { createBaseResponseSchema } = require('../../../schemas/response');
const { createBaseResponseDataPagination } = require('../../../schemas/response');

const activitySchema = {
  type: 'object',
  properties: {
    'id': {type: 'string'},
    'skill': { type: 'string' },
    'title': { type: 'string' },
    'description': { type: 'string' },
    'startdate': { type: 'string'  },
    'enddate': { type: 'string'  },
    'participants': { 
      type: 'array', 
      items: {
        type: 'object',
        properties: {
          'id': {type: 'string'},
          'name': {type: 'string'},
          'profile': {type: 'string'},
          'skill': {type: 'string'},
        }
      }
    },
  }
}

const createActivityBodySchema = {
  ...activitySchema,
  required: [
    'skill',
    'title',
    'description',
    'startdate',
    'enddate',
    'participants',
  ],
  properties: {
    ...activitySchema.properties,
    'participants': { 
      type: 'array', 
      items: {
        type: 'string'
      }
    },
  }
}

const responseActivitySchema = createBaseResponseSchema(activitySchema);
const responseSingleActivitySchema = createBaseResponseSchema({
  type: 'object',
  properties: {
  'id': {type: 'string'},
  'skill_id': { type: 'string' },
  'skill_name': { type: 'string' },
  'title': { type: 'string' },
  'description': { type: 'string' },
  'startdate': { type: 'string'  },
  'enddate': { type: 'string'  },
  'participants': { 
      type: 'array', 
      items: {
        type: 'object',
        properties: {
          'id': {type: 'string'},
          'name': {type: 'string'},
          'profile': {type: 'string'},
          'skill': {type: 'string'},
        }
      }
    },
  }
});

const responseListActivitySchema = createBaseResponseDataPagination({
  type: 'array',
  items: {
    type: 'object',
    properties: {
    'id': {type: 'string'},
    'skill_id': { type: 'string' },
    'skill_name': { type: 'string' },
    'title': { type: 'string' },
    'description': { type: 'string' },
    'startdate': { type: 'string'  },
    'enddate': { type: 'string'  },
    'participants': { 
        type: 'array', 
        items: {
          type: 'object',
          properties: {
            'id': {type: 'string'},
            'name': {type: 'string'},
            'profile': {type: 'string'},
            'skill': {type: 'string'},
          }
        }
      },
    }
  }
});

module.exports ={ 
  responseActivitySchema,
  responseListActivitySchema,
  createActivityBodySchema,
  responseSingleActivitySchema
}