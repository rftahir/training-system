'use strict'
const { createActivityBodySchema, responseActivitySchema, responseListActivitySchema, responseSingleActivitySchema } = require('./schema'); 
const { ErrorResponseSchema } = require('../../../schemas/response');
const { BaseFilterSchema } = require('../../../schemas/request')
const { isDate } = require('../../../helpers')

module.exports = async function (fastify, opts) {
  fastify.post(
    '/',
    {
      onRequest: [fastify.authenticate],
      preHandler: [fastify.guard.role(['expert'])],
      schema: {
        body: createActivityBodySchema,
        response: {
          200: responseActivitySchema,
          422: ErrorResponseSchema,
          401: ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const {
        skill,
        title,
        description,
        startdate,
        enddate,
        participants
      } = req.body;


      try {
        if(!isDate(startdate) || !isDate(enddate))
          throw 'Data cannot be processed';

        if(participants.length == 0)
          throw 'Data cannot be processed';

        await fastify.knex.transaction(async function(trx){
          const [activity] = await trx.insert(
            {
              skill,
              title,
              description,
              startdate,
              enddate,
            },
            ['id']
          )
          .into('activities')
          .catch(function (error) {
            throw error;
          });

          for (let i = 0; i < participants.length; i++) {
             await trx.insert(
              {
                activity_id: activity.id,
                user_id: participants[i]
              },
            )
            .into('participants');
          }
        }).catch(function (error) {
          throw error;
        });

        return {
          message: "Create Success"
        };
      } catch (error) {
        console.log(error);
        return reply.status(422).send({
          statusCode: 422,
          message: 'Data cannot be processed'
        })
      }
    }
  )

  fastify.put(
    '/:id',
    {
      onRequest: [fastify.authenticate],
      preHandler: [fastify.guard.role(['expert'])],
      schema: {
        body: createActivityBodySchema,
        response: {
          200: responseActivitySchema,
          '4xx': ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const {
        skill,
        title,
        description,
        startdate,
        enddate,
        participants
      } = req.body;

      const { id } = req.params;


      try {
        if(!isDate(startdate) || !isDate(enddate))
          throw 'Data cannot be processed';

        if(participants.length == 0)
          throw 'Data cannot be processed';

        const currentData = fastify.knex('activities').where('id', id).first();

        if(!currentData) return reply.status(404).send({status: 404});


        await fastify.knex.transaction(async function(trx){
          
          await trx('participants').where('activity_id', id).del();

          const [activity] = await trx('activities')
                                  .where('id', id)
                                  .update(
                                    {
                                      skill,
                                      title,
                                      description,
                                      startdate,
                                      enddate,
                                    },
                                    ['id']
                                  )
                                  .catch(function (error) {
                                    throw error;
                                  });

          for (let i = 0; i < participants.length; i++) {
             await trx.insert(
              {
                activity_id: activity.id,
                user_id: participants[i]
              },
            )
            .into('participants');
          }
        }).catch(function (error) {
          throw error;
        });

        return {
          message: "Update Success"
        };
      } catch (error) {
        console.log(error);
        return reply.status(422).send({
          statusCode: 422,
          message: 'Data cannot be processed'
        })
      }
    }
  )

  fastify.delete(
    '/:id',
    {
      onRequest: [fastify.authenticate],
      preHandler: [fastify.guard.role(['expert'])],
      schema: {
        response: {
          200: responseActivitySchema,
          '4xx': ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const { id } = req.params;
      
      try {
        const currentData = fastify.knex('activities').where('id', id).first();

        if(!currentData) return reply.status(404).send({status: 404});

        await fastify.knex('activities').where('id', id).del().catch(err => {throw err});

        return {
          message: "Delete Success"
        };
      } catch (error) {
        console.log(error);
        return reply.status(422).send({
          statusCode: 422,
          message: 'Data cannot be processed'
        })
      }
    }
  )

  fastify.get(
    '/',
    {
      onRequest: [fastify.authenticate],
      schema: {
        querystring: BaseFilterSchema,
        response: {
          200: responseListActivitySchema,
          422: ErrorResponseSchema,
          401: ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const {
        perPage = 10,
        currentPage = 1,
        order = "title",
        orderType = "ASC",
        q = '',
        allData = false
      } = req.query;

      try {
        let activities = await fastify.knex('activities')
                                      .join('skills', 'activities.skill', 'skills.id')
                                      .select(
                                          'activities.id',
                                          'skills.id as skill_id',
                                          'skills.skill_name',
                                          'title',
                                          'description',
                                          'startdate',
                                          'enddate'
                                        )
                                      .where('title', 'like', `%${q}%`)
                                      .orWhere('skills.skill_name', 'like', `%${q}%`)
                                      .orderBy(order, orderType)
                                      .paginate({perPage, currentPage})
                                      .catch(err => {
                                        throw err;
                                      });
        if(allData){
          activities = await fastify.knex('activities')
                                      .join('skills', 'activities.skill', 'skills.id')
                                      .select(
                                          'activities.id',
                                          'skills.id as skill_id',
                                          'skills.skill_name',
                                          'title',
                                          'description',
                                          'startdate',
                                          'enddate'
                                        )
                                      .where('title', 'like', `%${q}%`)
                                      .orWhere('skills.skill_name', 'like', `%${q}%`)
                                      .orderBy(order, orderType)
                                      .catch(err => {
                                        throw err;
                                      });
        }

        for (const activity of activities.data) {
          activity.participants = await fastify.knex('participants')
                                            .join('users', 'participants.user_id', 'users.id')
                                            .join('skills', 'users.skill', 'skills.id')
                                            .select('users.id', 'name', 'profile', 'skills.skill_name as skill')
                                            .where('participants.activity_id', activity.id)
                                            .catch(err => {
                                              throw err;
                                            });

        }

        return {
          message: "success",
          data: activities.data,
          ...activities.pagination,
        };

      } catch (error) {
        console.log(error)
        return reply.status(422).send({
          statusCode: 422,
          message: 'Data cannot be processed'
        })
      }
    }
  )

  fastify.get(
    '/:id',
    {
      onRequest: [fastify.authenticate],
      schema: {
        response: {
          200: responseSingleActivitySchema,
          422: ErrorResponseSchema,
          401: ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const { id } = req.params;

      try {
        let activity = await fastify.knex('activities')
                             .join('skills', 'activities.skill', 'skills.id')
                             .select(
                                'activities.id',
                                'skills.id as skill_id',
                                'skills.skill_name',
                                'title',
                                'description',
                                'startdate',
                                'enddate'
                              )
                              .where('activities.id', id)
                              .first()
                              .catch(function (err) {
                                throw err;
                              });
        if(!activity)
          return reply.status(404).send({
            statusCode: 404,
            message: 'Data not found'
          });

        activity.participants = await fastify.knex('participants')
                                .join('users', 'participants.user_id', 'users.id')
                                .join('skills', 'users.skill', 'skills.id')
                                .select('users.id', 'name', 'profile', 'skills.skill_name as skill')
                                .where('participants.activity_id', activity.id)
                                .catch(function (err) {
                                  throw err;
                                });

        return {
          message: "success",
          data: activity
        };              
      } catch (error) {
        console.log(error)
        return reply.status(422).send({
          statusCode: 422,
          message: 'Data cannot be processed'
        })
      }
    }
  )
}
