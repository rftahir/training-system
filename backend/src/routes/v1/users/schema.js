const { createBaseResponseSchema, createBaseResponseDataPagination } = require('../../../schemas/response');


const registerBodySchema = {
  type: 'object',
  properties: {
    'name': { type: 'string' },
    'email': { type: 'string' },
    'username': { type: 'string' },
    'password': { type: 'string' },
    'profile': { type: 'string', enum: ['board', 'expert', 'trainer', 'competitor'] },
    'skill': { type: 'string', nullable: true},
  }
}

const responseCreateUserSchema = createBaseResponseSchema(registerBodySchema);

const responseListUserSchema = createBaseResponseDataPagination({
  type: 'array',
  items: {
    type: 'object',
    properties: {
      'id': { type: 'string' },
      'name': { type: 'string' },
      'email': { type: 'string' },
      'username': { type: 'string' },
      'profile': { type: 'string' },
      'skill': { type: 'string' },
    }
  }
})

module.exports ={ 
  registerBodySchema,
  responseCreateUserSchema,
  responseListUserSchema
}