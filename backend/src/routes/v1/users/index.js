'use strict'
const { registerBodySchema, responseCreateUserSchema, responseListUserSchema } = require('./schema'); 
const { ErrorResponseSchema } = require('../../../schemas/response');
const { BaseFilterSchema } = require('../../../schemas/request')

const bcrypt = require('bcrypt');

module.exports = async function (fastify, opts) {
  fastify.post(
    '/',
    {
      onRequest: [fastify.authenticate],
      preHandler: [fastify.guard.role(['board'])],
      schema: {
        body: registerBodySchema,
        response: {
          200: responseCreateUserSchema,
          422: ErrorResponseSchema,
          401: ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const {name, email, username, password, profile, skill} = req.body;

      try {
         const encryptedPassword = await bcrypt.hash(password, 10);

        await fastify.knex('users').insert({
          name,
          email,
          username,
          password: encryptedPassword,
          profile,
          skill
        }).catch(err => {
          return reply.status(422).send(err)
        });

        return {
          message: "Create Success"
        };

      } catch (error) {
        console.log(error);
        return reply.status(422).send({
          statusCode: 422,
          message: 'Data cannot be processed'
        })
      }
    }
  )

   fastify.get(
    '/',
    {
      onRequest: [fastify.authenticate],
      schema: {
        querystring: BaseFilterSchema,
        response: {
          200: responseListUserSchema,
          422: ErrorResponseSchema,
          401: ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const {
        perPage = 10,
        currentPage = 1,
        order = "name",
        orderType = "ASC",
        q = '',
      } = req.query;

      try {

        let users = await fastify.knex('users')
                                      .select(
                                        'users.id',
                                        'name',
                                        'email',
                                        'username',
                                        'profile',
                                        'skills.skill_name as skill'
                                      )
                                      .leftJoin('skills', 'users.skill', 'skills.id')
                                      .where('name', 'like', `%${q}%`)
                                      .orderBy(order, orderType)
                                      .paginate({perPage, currentPage})
                                      .catch(err => {
                                        throw err;
                                      });

        return {
          message: "success",
          data: users.data,
          ...users.pagination,
        };

      } catch (error) {
        console.log(error)
        return reply.status(422).send({
          statusCode: 422,
          message: 'Data cannot be processed'
        })
      }
    }
  )
}
