'use strict'
const { BaseFilterSchema } = require('../../../schemas/request')
const { ErrorResponseSchema } = require('../../../schemas/response');
const { responseListSkillsSchema } = require('./schema'); 

module.exports = async function (fastify, opts) {
  fastify.get(
    '/',
    {
      onRequest: [fastify.authenticate],
      schema: {
        querystring: BaseFilterSchema,
        response: {
          200: responseListSkillsSchema,
          422: ErrorResponseSchema,
          401: ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const {
        perPage = 10,
        currentPage = 1,
        order = "skill_name",
        orderType = "ASC",
        q = '',
        allData = false
      } = req.query;

      try {
        let skills = await fastify.knex('skills')
                              .select('id', 'skill_name')
                              .where('skill_name', 'like', `%${q}%`)
                              .orderBy(order, orderType)
                              .paginate({perPage, currentPage})
                              .catch(err => {
                                return reply.status(422).send({
                                  statusCode: 422,
                                  message: 'Data cannot be processed'
                                })
                              });
        if(allData){
            skills = await fastify.knex('skills')
                      .select('id', 'skill_name')
                      .where('skill_name', 'like', `%${q}%`)
                      .orderBy(order, orderType)
                      .catch(err => {
                        return reply.status(422).send({
                          statusCode: 422,
                          message: 'Data cannot be processed'
                        })
                      });
            
            return {
              message: "success",
              data: skills,
            };
        }

        return {
          message: "success",
          data: skills.data,
          ...skills.pagination,
        };

      } catch (error) {
        console.log(error)
        return reply.status(422).send(error);
      }
    }
  )
}
