const { createBaseResponseDataPagination } = require('../../../schemas/response');


const responseListSkillsSchema = createBaseResponseDataPagination({
  type: 'array',
  items: {
    type: 'object',
    properties: {
      'id': { type: 'string' },
      'skill_name': { type: 'string' },
    }
  }
});

module.exports ={ 
  responseListSkillsSchema
}