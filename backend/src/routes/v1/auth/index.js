'use strict'
const { loginBodySchema, loginResponseSchema } = require('./schema'); 
const { ErrorResponseSchema } = require('../../../schemas/response');
const { setRedis, getRedis } = require('../../../helpers');
const bcrypt = require('bcrypt');

module.exports = async function (fastify, opts) {
  fastify.post(
    '/login',
    {
      schema: {
        body: loginBodySchema,
        response: {
          200: loginResponseSchema,
          422: ErrorResponseSchema,
          401: ErrorResponseSchema
        }
      }
    },
    async function (req, reply) {
      const {username, password} = req.body;

      try {

        const user = await fastify.knex('users').where('username', username).first('id', 'username', 'password', 'profile')
        const validatePassword = user ? await bcrypt.compare(password, user.password) : null ;
        
        if (!user || !validatePassword)
          throw {
            statusCode: 401,
            error: 'INVALID_LOGIN',
            message: 'Invalid Login',
          };

        const payload = {
          id: user.id,
          profile: user.profile
        }

        return {
          message: "success",
          data: {
            token: fastify.jwt.sign(payload, { expiresIn: '1d'}),
            name: user.name,
            skill: user.skill,
            profile: user.profile
          }
        };

      } catch (error) {
        console.log(error);
        if (error.statusCode) return reply.status(error.statusCode).send(error);

        return reply.status(422).send(error);
      }
    }
  )

  fastify.get(
    '/logout',
    {
      onRequest: [fastify.authenticate],
    },
    async function(req, reply) {
      const auth = req.headers.authorization;
      const token = auth.split(' ')[1];

      try {  
        let currentBlacklist = await getRedis(fastify, 'blacklist');

        if(!currentBlacklist){
          currentBlacklist = [];
        }else{
          currentBlacklist = JSON.parse(currentBlacklist);
        }

        if(!currentBlacklist.includes(token)) currentBlacklist.push(token);

        await setRedis(fastify, 'blacklist', JSON.stringify(currentBlacklist));

        return {
          message: 'Logout Success',
        }
      } catch (error) {
        reply.status(422).send({
          status: 422,
          message: error
        })
      }

    }
  )
}
