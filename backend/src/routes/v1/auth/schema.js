const { createBaseResponseSchema } = require('../../../schemas/response');

const loginBodySchema = {
  type: 'object',
  required: ['username', 'password'],
  properties: {
    username: { type: 'string' },
    password: { type: 'string' },
  }
}

const loginResponseSchema = createBaseResponseSchema({
  type: 'object',
  properties: {
    token: { type: 'string' },
    profile: { type: 'string' }
  }
})

module.exports = {
  loginBodySchema,
  loginResponseSchema
}