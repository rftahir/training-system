module.exports = {
  isDate: function(date) {
    return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
  },
  getRedis: function(fastify, redisKey){
    return new Promise(function(resolve, reject){
      fastify.redis.get(redisKey, (err, val) => {
        if(err) reject(err)
        resolve(val);
      });
    })
  },
  setRedis: function(fastify, redisKey, redisValue){
    return new Promise(function(resolve, reject){
      fastify.redis.set(redisKey, redisValue, (err, val) => {
        if(err) reject(err);
        
        resolve(val);
      });
    })
  }
}